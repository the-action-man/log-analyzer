# Log Analyzer

Technologies:

* Java 8
* Maven 3.3.9+

Code style file:
https://github.com/google/styleguide/blob/gh-pages/intellij-java-google-style.xml

How to run (in Windows):

1. Create some folder (e.g. "log-analyzer")
2. Create `log-analyzer.jar` artifact what contains all dependencies
3. Put `log-analyzer.jar` into "log-analyzer" folder
4. Put files of `<project-root>/scripts` folder into "log-analyzer" folder
5. Add "log-analyzer" folder into System Environment Variable "Path"
6. Enter `la --help` in command line

## Input Parameters

### Requirements for Log Files in Input Directory

1. Log files are parts of separated log
2. Input directory should contains log files with names `*.log` or `*.log.XX` where XX is order number
3. Directory contains only one file with name `*.log`
4. Line format: `yyyy/MM/dd HH:mm:ss.SSS <username without spaces> - <message>`

### Other

Enter `la --help` in command line to see other parameter requirements
