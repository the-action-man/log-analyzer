package org.theactionman.loganalyzer.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Converter {
  public static LocalDateTime strToLocalDateTime(String value, String format) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    return LocalDateTime.parse(value, formatter);
  }

  public static String localDateTimeToStr(LocalDateTime localDateTime, String format) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    return localDateTime.format(formatter);
  }
}
