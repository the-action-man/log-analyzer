package org.theactionman.loganalyzer.util;

import java.io.File;
import java.io.FilenameFilter;

public class LogFileNameFilter implements FilenameFilter {

  public static final String REGEXP_LOG = ".*\\.log$";
  public static final String REGEXP_LOG_NUMBER = ".*\\.log\\.\\d{2}$";
  public static final String REGEXP = "(" + REGEXP_LOG + ")|(" + REGEXP_LOG_NUMBER + ")";

  private String regexp;

  public LogFileNameFilter() {
    regexp = LogFileNameFilter.REGEXP;
  }

  public LogFileNameFilter(String regexp) {
    this.regexp = regexp;
  }

  public boolean accept(File dir, String name) {
    return name.matches(regexp);
  }
}
