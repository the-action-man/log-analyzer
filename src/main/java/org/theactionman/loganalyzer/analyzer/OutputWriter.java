package org.theactionman.loganalyzer.analyzer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.theactionman.loganalyzer.exception.LogAnalyserException;

public class OutputWriter {

  private static OutputWriter instance;
  private static BufferedWriter writer;
  private static String pathToFile;

  private OutputWriter(String pathToFile) {
    OutputWriter.pathToFile = pathToFile;
    Path path = Paths.get(OutputWriter.pathToFile);
    try {
      writer = Files.newBufferedWriter(path);
    } catch (IOException e) {
      throw new LogAnalyserException("Writing to '" + pathToFile + "' file cannot be execute", e);
    }
  }

  public static void getInstance(String pathToFile) {
    if (instance == null) {
      instance = new OutputWriter(pathToFile);
    }
  }

  public static void write(String line) {
    try {
      writer.write(line);
      writer.newLine();
    } catch (IOException e) {
      throw new LogAnalyserException("Line writing to '" + pathToFile + "' file cannot be execute",
          e);
    }
  }

  public static void close() {
    try {
      writer.close();
    } catch (IOException e) {
      throw new LogAnalyserException("File writer for '" + pathToFile + "' file cannot be closed",
          e);
    }
  }
}
