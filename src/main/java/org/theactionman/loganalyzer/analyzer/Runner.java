package org.theactionman.loganalyzer.analyzer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.stream.Stream;
import org.theactionman.loganalyzer.exception.LogAnalyserException;
import org.theactionman.loganalyzer.util.LogFileNameFilter;

public class Runner {

  private static Runner instance;
  private static String pathToInputDir;
  private static String pathToOutputFile;

  /* filter */
  private static LocalDateTime startMoment;
  private static LocalDateTime endMoment;
  private static String userForFilter;
  private static String msgPattern;

  /* grouping */
  private static boolean isGroupByUser;
  private static GroupTimeUnit timeUnit;

  private Runner(String pathToInputDir, String pathToOutputFile, String userForFilter,
      LocalDateTime startMoment, LocalDateTime endMoment, String msgPattern,
      boolean isGroupByUser, GroupTimeUnit timeUnit) {
    Runner.pathToInputDir = pathToInputDir;
    Runner.pathToOutputFile = pathToOutputFile;
    Runner.userForFilter = userForFilter;
    Runner.startMoment = startMoment;
    Runner.endMoment = endMoment;
    Runner.msgPattern = msgPattern;
    Runner.isGroupByUser = isGroupByUser;
    Runner.timeUnit = timeUnit;
  }

  public static void getInstance(String pathToInputDir, String pathToOutputFile,
      String userForFilter, LocalDateTime startMoment, LocalDateTime endMoment, String msgPattern,
      boolean isGroupByUser, GroupTimeUnit timeUnit) {
    if (instance == null) {
      instance = new Runner(pathToInputDir, pathToOutputFile, userForFilter,
          startMoment, endMoment, msgPattern, isGroupByUser, timeUnit);
    }
  }

  public static void analyse() {
    OutputWriter.getInstance(pathToOutputFile);
    Statist.getInstance(isGroupByUser, timeUnit);
    System.out.println("Processing...");

    File[] logFiles = getLogFiles();
    for (File file : logFiles) {
      try {
        System.out.println("File: " + file.getAbsolutePath());
        analyzeFile(file.getAbsolutePath());
      } catch (IOException e) {
        throw new LogAnalyserException("Error in reading of '" + file.getAbsolutePath() + "' file",
            e);
      }
    }

    OutputWriter.close();
    Statist.showStatistic();
    System.out.println("Processing is completed");
  }

  private static void analyzeFile(String pathToFile) throws IOException {
    Path path = Paths.get(pathToFile);
    try (Stream<String> stream = Files.lines(path)) {
      stream
          .filter(Runner::filterOutAndGroup)
          .forEach(OutputWriter::write);
    }
  }

  private static boolean filterOutAndGroup(final String line) {
    LogLineData lineData = LogLineHandler.parseLine(line);
    if (startMoment != null) {
      if (!startMoment.isBefore(lineData.getTimestamp())) {
        return false;
      }
    }
    if (endMoment != null) {
      if (!endMoment.isAfter(lineData.getTimestamp())) {
        return false;
      }
    }
    if (userForFilter != null) {
      if (!userForFilter.equals(lineData.getUser())) {
        return false;
      }
    }
    if (msgPattern != null) {
      if (!lineData.getMessage().matches(msgPattern)) {
        return false;
      }
    }
    Statist.process(lineData);
    return true;
  }

  private static File[] getLogFiles() {
    File dir = new File(pathToInputDir);
    File[] logNumberFile = dir
        .listFiles(new LogFileNameFilter(LogFileNameFilter.REGEXP_LOG_NUMBER));
    File[] logFile = dir.listFiles(new LogFileNameFilter(LogFileNameFilter.REGEXP_LOG));

    File[] files = new File[logNumberFile.length + 1];
    for (int i = 0; i < logNumberFile.length; i++) {
      files[i] = logNumberFile[i];
    }
    files[logNumberFile.length] = logFile[0]; // log file without number contains last records
    return files;
  }
}
