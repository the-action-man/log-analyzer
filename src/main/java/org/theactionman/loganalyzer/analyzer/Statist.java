package org.theactionman.loganalyzer.analyzer;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import org.theactionman.loganalyzer.exception.LogAnalyserException;
import org.theactionman.loganalyzer.util.Converter;

public class Statist {

  private static final String GROUP_SEPARATOR = "==========================================";
  private static final String SEPARATOR = "-----------------------------------------";
  private static Statist instance;
  private static boolean isGroupByUser;
  private static GroupTimeUnit timeUnit;
  private static Map<String, Long> userStat = new HashMap<>();
  private static Map<LocalDateTime, Long> timeUnitStat = new HashMap<>();

  private Statist(boolean isGroupByUser, GroupTimeUnit timeUnit) {
    Statist.isGroupByUser = isGroupByUser;
    Statist.timeUnit = timeUnit;
  }

  public static void getInstance(boolean isGroupByUser, GroupTimeUnit timeUnit) {
    if (instance == null) {
      instance = new Statist(isGroupByUser, timeUnit);
    }
  }

  public static void process(final LogLineData line) {
    if (isGroupByUser) {
      String key = line.getUser();
      if (userStat.containsKey(key)) {
        userStat.replace(key, userStat.get(key) + 1);
      } else {
        userStat.put(line.getUser(), 1L);
      }
    }
    if (timeUnit != null) {
      LocalDateTime key = defineTimeUnitPeriod(line.getTimestamp());
      if (timeUnitStat.containsKey(key)) {
        timeUnitStat.replace(key, timeUnitStat.get(key) + 1);
      } else {
        timeUnitStat.put(key, 1L);
      }
    }
  }

  public static void showStatistic() {
    if (isGroupByUser) {
      System.out.println(GROUP_SEPARATOR);
      System.out.println("Username --> Quantity of Records");
      System.out.println(SEPARATOR);
      if (userStat.size() > 0) {
        userStat.forEach((user, quantity) -> System.out.println(user + " --> " + quantity));
      } else {
        System.out.println("Nothing");
      }
    }
    if (timeUnit != null) {
      System.out.println(GROUP_SEPARATOR);
      System.out.println(timeUnit + " --> Quantity of Records");
      System.out.println(SEPARATOR);
      if (timeUnitStat.size() > 0) {
        timeUnitStat.forEach((period, quantity) ->
            System.out.println(convertToOutputStr(period) + " --> " + quantity));
      } else {
        System.out.println("Nothing");
      }
    }
    if (isGroupByUser || timeUnit != null) {
      System.out.println(GROUP_SEPARATOR);
    }
  }

  private static String convertToOutputStr(LocalDateTime localDateTime) {
    String format;
    if (timeUnit == GroupTimeUnit.MONTH) {
      format = "yyyy/MM";
    } else if (timeUnit == GroupTimeUnit.DAY) {
      format = "yyyy/MM/dd";
    } else if (timeUnit == GroupTimeUnit.HOUR) {
      format = "yyyy/MM/dd HH";
    } else {
      throw new LogAnalyserException("'" + timeUnit + "' time unit is unsupported by this tool");
    }

    return Converter.localDateTimeToStr(localDateTime, format);
  }

  private static LocalDateTime defineTimeUnitPeriod(final LocalDateTime timestamp) {
    if (timeUnit == GroupTimeUnit.MONTH) {
      return LocalDateTime.of(
          timestamp.getYear(),
          timestamp.getMonth(),
          1,
          0,
          0);
    }
    if (timeUnit == GroupTimeUnit.DAY) {
      return LocalDateTime.of(
          timestamp.getYear(),
          timestamp.getMonth(),
          timestamp.getDayOfMonth(),
          0,
          0);
    }
    if (timeUnit == GroupTimeUnit.HOUR) {
      return LocalDateTime.of(
          timestamp.getYear(),
          timestamp.getMonth(),
          timestamp.getDayOfMonth(),
          timestamp.getHour(),
          0);
    }
    throw new LogAnalyserException("'" + timeUnit + "' time unit is unsupported by this tool");
  }
}
