package org.theactionman.loganalyzer.analyzer;

public enum GroupTimeUnit {
  HOUR,
  DAY,
  MONTH
}
