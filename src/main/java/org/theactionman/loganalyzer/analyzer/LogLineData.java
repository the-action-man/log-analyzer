package org.theactionman.loganalyzer.analyzer;

import java.time.LocalDateTime;

public class LogLineData {

  private LocalDateTime timestamp;
  private String user;
  private String message;

  public LogLineData(LocalDateTime timestamp, String user, String message) {
    this.timestamp = timestamp;
    this.user = user;
    this.message = message;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public String getUser() {
    return user;
  }

  public String getMessage() {
    return message;
  }
}
