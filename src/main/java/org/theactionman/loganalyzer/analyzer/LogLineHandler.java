package org.theactionman.loganalyzer.analyzer;

import java.time.LocalDateTime;
import org.theactionman.loganalyzer.util.Converter;

public class LogLineHandler {

  public static final String TIMESTAMP_FORMAT = "yyyy/MM/dd HH:mm:ss.SSS";

  public static LogLineData parseLine(final String line) {
    String strTimestamp = line.substring(0, 23);
    LocalDateTime timestamp = Converter.strToLocalDateTime(strTimestamp, TIMESTAMP_FORMAT);

    int indexOfSpace = line.indexOf(" ", 24);
    String user = line.substring(24, indexOfSpace);

    String message = line.substring(indexOfSpace + 3, line.length());

    return new LogLineData(timestamp, user, message);
  }
}
