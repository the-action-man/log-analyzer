package org.theactionman.loganalyzer.cli;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.theactionman.loganalyzer.analyzer.GroupTimeUnit;
import org.theactionman.loganalyzer.analyzer.LogLineHandler;
import org.theactionman.loganalyzer.analyzer.Runner;
import org.theactionman.loganalyzer.util.Converter;
import org.theactionman.loganalyzer.util.LogFileNameFilter;

public class LogAnalyzer {

  private static final String USAGE = "la";
  private static String pathToInputDir;
  private static String pathToOutputFile;

  /* filter */
  private static LocalDateTime startMoment;
  private static LocalDateTime endMoment;
  private static String userForFilter;
  private static String msgPattern;

  /* grouping */
  private static boolean isGroupByUser = false;
  private static GroupTimeUnit timeUnit;

  public static void main(String args[]) {
    CommandLineParser parser = new DefaultParser();
    Options options = new Options();
    options.addOption("h", "help", false, "Prints this message");
    options.addOption("i", "input", true, "Absolute path to directory with log files");
    options.addOption("o", "output", true, "Absolute path to output file");
    options.addOption("f", "filter-user", true,
        "Username for filter. Username should be without spaces");
    options.addOption("s", "filter-start", true,
        "Start moment for filter. Format: \"" + LogLineHandler.TIMESTAMP_FORMAT + "\"");
    options.addOption("e", "filter-end", true,
        "End moment for filter. Format: \"" + LogLineHandler.TIMESTAMP_FORMAT + "\"");
    options.addOption("p", "filter-pattern", true,
        "Message pattern for filter. Example: \"^Enter.*\"");
    options.addOption("g", "group-user", false, "Group by username");
    options.addOption("u", "time-unit", true, "Group by time unit. Values: "
        + GroupTimeUnit.HOUR.toString().toLowerCase() + ", "
        + GroupTimeUnit.DAY.toString().toLowerCase() + ", "
        + GroupTimeUnit.MONTH.toString().toLowerCase());

    CommandLine commandLine;
    HelpFormatter helpFormatter = new HelpFormatter();
    try {
      commandLine = parser.parse(options, args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      helpFormatter.printHelp(USAGE, options);
      return;
    }

    if (commandLine.hasOption("h") || commandLine.getOptions().length == 0) {
      helpFormatter.printHelp(USAGE, options);
      return;
    }

    if (handleOptionInput(commandLine) &&
        handleOptionOutput(commandLine) &&
        handleOptionUserForFilter(commandLine) &&
        handleOptionStartMomentForFilter(commandLine) &&
        handleOptionEndMomentForFilter(commandLine) &&
        handleOptionMsgPatternForFilter(commandLine) &&
        handleOptionUserForGroup(commandLine) &&
        handleOptionTimeUnitForGroup(commandLine)) {

      Runner.getInstance(
          pathToInputDir,
          pathToOutputFile,
          userForFilter,
          startMoment,
          endMoment,
          msgPattern,
          isGroupByUser,
          timeUnit
      );
      Runner.analyse();
    }
  }

  @SuppressWarnings("ConstantConditions")
  private static boolean handleOptionInput(final CommandLine commandLine) {
    if (commandLine.hasOption("i")) {
      pathToInputDir = commandLine.getOptionValue("i");
      File file = new File(pathToInputDir);
      if (!file.exists() || !file.isDirectory()) {
        System.out
            .println(
                getWrongOptionMsg("i") + "'" + pathToInputDir + "' directory does not exist");
        return false;
      }
      if (file.listFiles(new LogFileNameFilter()).length == 0) {
        System.out.println(
            getWrongOptionMsg("i") + "'" + pathToInputDir + "' directory does not contain files "
                + "matched '" + LogFileNameFilter.REGEXP + "' regexp");
        return false;
      }
      if (file.listFiles(new LogFileNameFilter(LogFileNameFilter.REGEXP_LOG)).length > 1) {
        System.out.println(
            getWrongOptionMsg("i") + "'" + pathToInputDir
                + "' directory contains more than one file "
                + "matched '" + LogFileNameFilter.REGEXP + "' regexp");
        return false;
      }
    }
    return true;
  }

  private static boolean handleOptionOutput(final CommandLine commandLine) {
    if (commandLine.hasOption("o")) {
      pathToOutputFile = commandLine.getOptionValue("o");
      File file = new File(pathToOutputFile);
      try {
        if (!file.getParentFile().exists()) {
          System.out.println(getWrongOptionMsg("o") + "'" + pathToOutputFile + "' path is wrong");
          return false;
        }
      } catch (Exception e) {
        System.out.println(getWrongOptionMsg("o") + "'" + pathToOutputFile + "' path is wrong");
        return false;
      }
      //TODO uncomment
//      if (file.exists()) {
//        System.out.println(getWrongOptionMsg("o") + "'" + pathToOutputFile + "' file exists");
//        return false;
//      }
    }
    return true;
  }

  private static boolean handleOptionUserForFilter(final CommandLine commandLine) {
    if (commandLine.hasOption("f")) {
      userForFilter = commandLine.getOptionValue("f");
      if (userForFilter.contains(" ")) {
        System.out
            .println(getWrongOptionMsg("f") + "'" + userForFilter + "' username contains space");
        return false;
      }
    }
    return true;
  }

  private static boolean handleOptionStartMomentForFilter(final CommandLine commandLine) {
    if (commandLine.hasOption("s")) {
      String value = commandLine.getOptionValue("s");
      if (!value.matches("\\d{4}/\\d{2}/\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}\\.\\d{3}")) {
        System.out.println(getWrongOptionMsg("s") + "'" + value
            + "' does not match format \"" + LogLineHandler.TIMESTAMP_FORMAT + "\"");
        return false;
      }
      try {
        startMoment = Converter.strToLocalDateTime(value, LogLineHandler.TIMESTAMP_FORMAT);
      } catch (DateTimeParseException e) {
        System.out.println(getWrongOptionMsg("s") + e.getMessage());
        return false;
      }
    }
    return true;
  }

  private static boolean handleOptionEndMomentForFilter(final CommandLine commandLine) {
    if (commandLine.hasOption("e")) {
      String value = commandLine.getOptionValue("e");
      if (!value.matches("\\d{4}/\\d{2}/\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}\\.\\d{3}")) {
        System.out.println(getWrongOptionMsg("e") + "'" + value
            + "' does not match format \"" + LogLineHandler.TIMESTAMP_FORMAT + "\"");
        return false;
      }
      try {
        endMoment = Converter.strToLocalDateTime(value, LogLineHandler.TIMESTAMP_FORMAT);
      } catch (DateTimeParseException e) {
        System.out.println(getWrongOptionMsg("e") + e.getMessage());
        return false;
      }
    }
    return true;
  }

  private static boolean handleOptionMsgPatternForFilter(final CommandLine commandLine) {
    if (commandLine.hasOption("p")) {
      msgPattern = commandLine.getOptionValue("p");
    }
    return true;
  }

  private static boolean handleOptionUserForGroup(final CommandLine commandLine) {
    if (commandLine.hasOption("g")) {
      isGroupByUser = true;
    }
    return true;
  }

  private static boolean handleOptionTimeUnitForGroup(final CommandLine commandLine) {
    if (commandLine.hasOption("u")) {
      String value = commandLine.getOptionValue("u");
      try {
        timeUnit = GroupTimeUnit.valueOf(value.toUpperCase());
      } catch (IllegalArgumentException e) {
        System.out.println(
            getWrongOptionMsg("u") + "'" + value + "' time unit is wrong. Expected values: "
                + GroupTimeUnit.HOUR.toString().toLowerCase() + ", "
                + GroupTimeUnit.DAY.toString().toLowerCase() + ", "
                + GroupTimeUnit.MONTH.toString().toLowerCase());
        return false;
      }
    }
    return true;
  }

  private static String getWrongOptionMsg(String option) {
    return String.format("Wrong value of option '-%s': ", option);
  }
}
