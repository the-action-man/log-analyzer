package org.theactionman.loganalyzer.exception;

public class LogAnalyserException extends RuntimeException {

  public LogAnalyserException(String message) {
    super(message);
  }

  public LogAnalyserException(String message, Throwable cause) {
    super(message, cause);
  }
}
